#!/bin/bash

EMAIL="ariddle@mac.com"
YUMTMP="/tmp/yum-check-update.tmp"
YUM="/usr/bin/yum"
$YUM check-update -q | awk '{print $1}' > $YUMTMP

DATE=$(date)
NUMBERS=$(cat $YUMTMP | wc -l)
UPDATES=$(cat $YUMTMP)

echo "
There are $NUMBERS updates available on host $HOSTNAME at $DATE

The available updates are:
$UPDATES"
