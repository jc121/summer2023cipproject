import subprocess
import os

command = "dnf check-update"

def is_command(name):
    from shutil import which
    return which(name) is not None

if not is_command("sendmail"):
    print("sendmail could not be found, please run setup.sh first")


else:
    print("sendmail is already installed")

process = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
(output, err) = process.communicate()

output = output.decode('utf8').split("\n")
formatted_output = ""

package_size = 0
for line in output:
    parts = line.split()

    if len(parts) == 3:
        package_size += 1
        formatted_line = f"Package: {parts[0]}\nVersion: {parts[1]}\nOS: {parts[2]}\n"
        formatted_output += formatted_line
        formatted_output += '---------------------------------------------------\n'
    else:
	      formatted_output += line
	      formatted_output += "\n"

formatted_output += f"Packages can be upgraded: {package_size}\n"



with open('output.txt', 'w') as f:
    f.write(formatted_output)

#emails = ['jc121@illinois.edu', 'yulin4@illinois.edu', 'aaravs2@illinois.edu', 'iaa6@illinois.edu', 'rriddle2@illinois.edu', 'cip2023summer@outlook.com']
emails = ['iaa6@illinois.edu']

subject = 'DNF Check Update Output'
from_email = 'cip2023summer@gmail.com'


sendmail_command = f"echo 'Subject: {subject}\nFrom: {from_email}\nTo: {', '.join(emails)}\n\n{formatted_output}' | /usr/sbin/sendmail -t"



process = subprocess.Popen(sendmail_command, stdout=subprocess.PIPE, shell=True)


