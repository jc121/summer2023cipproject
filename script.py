import subprocess
import datetime
import smtplib


# Configuration
nodes = ["localhost"]
log_file = "log.txt"

def check_updates(node):
    if node == "localhost":
        result = subprocess.run(['yum', 'check-update'], stdout=subprocess.PIPE, universal_newlines=True)
    else:
        # If remote, use SSH
        result = subprocess.run(['ssh', node, 'yum', 'check-update'], stdout=subprocess.PIPE, universal_newlines=True)
    return result.stdout

output_content = ""
for node in nodes:
    updates = check_updates(node)
    
    header = f"Node: {node}\nDate: {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}\n"
    output_content += header + updates + "\n\n"

# Write to the log file (overwrites if it already exists)
with open(log_file, "w") as file:
    file.write(output_content)

host = 'localhost'
server = smtplib.SMTP(host, 1025)
FROM = "cip2023summer@gmail.com"
TO = "aaravs2@illinois.edu"
MSG = "Subject: Test email python\n\nBody of your message!"
server.sendmail(FROM, TO, MSG)

server.quit()
print ("Email Send")
