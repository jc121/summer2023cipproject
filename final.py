import argparse     # to parse arguments from the wrapper
import syslog       # to send output to syslog
import subprocess   # to run processes in linux
import socket       # to access hostname
import datetime     # to format date-time

# Get the hostname of the system
hostname = socket.gethostname()

# Define the configurations for the script and their help logs
parser = argparse.ArgumentParser(description='Check updates on this system')
parser.add_argument("--email", nargs='+', type=str, help="Emails to send the output")
parser.add_argument("--compact", action="store_true", help="Display a quick list of packages sorted by install date")
parser.add_argument("--terminal", action="store_true", help="Print the output to the terminal")

args = parser.parse_args()

# Function to check if a command is available on the system
def is_command_available(name):
    from shutil import which
    return which(name) is not None

# Check if sendmail is installed when email option is used
if not is_command_available("sendmail") and args.email:
    print("Sendmail is not installed. Please install sendmail to send emails.")

# Parse packages from the Linux output into formatted dictionaries
def parse_packages(output_string):
    package_dict_list = []
    package_lines = output_string.strip().split("\n\n")

    # Define the default fields to keep for each package
    keep_fields = ["Release", "Name", "Summary", "Repository", "Version", "Install Date"]

    if args.compact:
        # If --compact flag is provided, only keep the "Name" field in the dictionary
        keep_fields = ["Name"]

    for package_info in package_lines:
        package_details = package_info.split("\n")
        package_dict = {}

        for detail in package_details:
            if ":" in detail:
                key, value = detail.split(":", 1)
                if key.strip() in keep_fields:
                    if "Version" in key:
                        package_dict["Update Version"] = value.strip()
                        continue
                    if "Release" in key:
                        # Append release information to the "Version" field
                        package_dict["Update Version"] += "-" + value.strip()
                        continue
                    package_dict[key.strip()] = value.strip()

        # Retrieve the install date of the package using 'rpm' command
        command_for_installdate = ['rpm', '-q', '--queryformat', '%{INSTALLTIME:date}', '--last', package_dict["Name"]]
        result = subprocess.run(command_for_installdate, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                universal_newlines=True)
        output_string = result.stdout
        date = output_string.split()[1:6]
        date_str = " ".join(date).strip()
        date_obj = datetime.datetime.strptime(date_str, '%d %b %Y %I:%M:%S %p')
        package_dict["Install Date"] = date_obj

        # Retrieve the current version of the package using 'rpm' command
        command_for_version = ['rpm', '-q', '--queryformat', '%{Version}-%{Release}', package_dict["Name"]]
        result = subprocess.run(command_for_version, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                universal_newlines=True)
        package_dict["Current Version"] = result.stdout.strip()

        package_dict_list.append(package_dict)

    return package_dict_list

# Replace 'dnf', 'info', and '--updates' with the actual command and its arguments
command = ['dnf', 'info', '--updates']
result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
output_string = result.stdout
final_dict = parse_packages(output_string)

formatted_output = ""
if len(final_dict) == 0:
    formatted_output = "All packages have been updated."
else:
    formatted_output += "Total packages need upgrade: " + str(len(final_dict)) + "\n" + "-" * 80 + "\n"
    for package in final_dict:
        # Specify the desired order of keys
        if not args.compact:
            keys_order = ["Name", "Install Date", "Current Version", "Update Version", "Repository", "Summary"]
        else:
            # If --compact flag is provided, print only the Name field
            keys_order = ["Name", "Install Date", "Current Version"]
        
        for key in keys_order:
            val = package.get(key, "")
            if isinstance(val, datetime.datetime):
                val = val.strftime('%d %b %Y %I:%M:%S %p')
            formatted_output += key + (16-len(key))* " " +": " + str(val) + "\n"

        formatted_output += "-" * 80 + "\n"

    if args.terminal:
        # Print the formatted output if --terminal flag is provided
        print(formatted_output)

# Logging to syslog
if args.terminal or args.email:
    # If either --terminal or --email flags are provided, log the output to syslog
    syslog.openlog("PackageMonitor")
    syslog.syslog(syslog.LOG_INFO, formatted_output)

if args.email:
    # If --email flag is provided, send the output via email
    subject = 'DNF Check Update Output on ' + hostname
    from_email = hostname
    to_email = args.email
    sendmail_command = f"echo 'Subject: {subject}\nFrom: {from_email}\nTo: {', '.join(to_email)}\n\n{formatted_output}' | /usr/sbin/sendmail -t"
    process = subprocess.Popen(sendmail_command, stdout=subprocess.PIPE, shell=True)
else:
    print('No email provided, just print the output and syslog, if email needed, please add --email address')