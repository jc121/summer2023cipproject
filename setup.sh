#!/bin/bash

# Script to install sendmail package
# Since we only need sendmail package, This script is been made strightfoward

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Install sendmail
echo "Installing sendmail..."
dnf install -y sendmail

if ! command -v sendmail &> /dev/null
then
    echo "Error: sendmail could not be installed"
    exit 1
fi

echo "sendmail successfully installed!"

# Start and enable service
systemctl start sendmail
systemctl enable sendmail

echo "sendmail service started and enabled!"

