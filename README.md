## 1. Introduction
- This Python script was written by CIP Summer 2023 interns, to streamline package update reports for admin-managed systems.
- Given the admin's email, it will send a formatted list of packages that need updates at a cron-specified cadence.
- It will also print this to the terminal and send it to syslogs by default.
- This information is pulled through dnf info --updates and is processed as a dictionary. Comments in the script should provide more context.
- The wrapper file will be used to input the email address, formatting style, and whether or not you would like to terminal output.

## 2. Requirements
Python 3.6 and higher
Sendmail
## 3. Installation
1. Download the Python script here →
1. Download the wrapper file here →
1. Install sendmail package by: sudo dnf install sendmail
1. Dump them both in /etc/cron.x (hourly, daily, weekly as you wish) 
1. Edit the wrapper file and add the email address you wish to send to as --email <address> after the script file
1. Edit the finaly.py file by adding "#!/bin/python3" on top of the file

## 4. Configuration

--email <your-email-address>

Input the email address that you would like to receive the email listing package updates at.

--compact

This outputs a list of packages and their install dates only chronologically by the installation date.
## 5. Usage
### Cron
- Copy available_package_updates.py and available_updates_wrapper.sh in the appropriate cron directory (/etc/cron.d/cron.daily, /etc/cron.d/cron.weekly, etc). This will run the script at specified intervals.
- This sends an email to the configured address and writes output to syslog. 

### Command line
- Basic usage: python3 available_package_updates.py
- - This will print information (name, version, repo, summary, install date, currently installed version) about available package updates to STDOUT, and will write information to syslog.
- Example output:
- - see: https://wiki.ncsa.illinois.edu/display/NCSACIP/System+Admin+Tool+Documentation
- Command line options
- - --email address@illinois.edu    Manually send an email with available packages to a specified address.
- - --compact                                  Condensed 
output sorted by last package update date.
## 6. Functionality
- This tool works via a python script placed in one of the directories supported by the systemd cron service. Systemd will run the script at the frequency setup by the system administrator (daily, weekly, etc.) and this script will send e-mail indicating the host and the packages with available updates, along with some summary information.
- Alternatively, the python script can be run directly from the command line of the system to display the same information to the terminal for review.
- Also supports command line options to help provide a more concise overview of the updates available.
- Services used:
- This script makes use of sendmail to send e-mails.
- To run the script on a schedule, this script needs to make use of the systemd cron service.
- Requires Python3. The script has been tested with Python 3.6 and 3.9. It uses the sys and os packages of Python3.

## 7. Error Handling and Limitations
- Invalid email or email not updated.
- In order to send an email, the user must configure a valid email address in the script to mail to. This is located in available_package_updates.py and is the default email the script will send to if run as a cron job.
- Currently there is no error checking for valid emails. An invalid email will fail to send.
- No package updates are available.
- If there are no package updates available the script will indicate 0 updates available.

## 8. Security
- Since we are simply sending an email to an illinois.edu account, there is always the risk of the specified admin's system being compromised. 
- Still, this does not directly affect the security of the nodes, unless access to them is also compromised.

## 9. Examples
- sudo python3 final.py --email address@illinois.edu : will sent the list of available packages on a node to update to address@illinois.edu email address. Note: no need for quotation marks("") for email. 
- sudo python3 final.py --terminal: will print the output to the terminal instead of sent it to an email address.
- 
- of course, you can also run "sudo python3 final.py --email yulin4@illinois.edu --terminal" sent it to an email address address@illinois.edu and display the output to the terminal.
- sudo python3 final.py --email address@illinois.edu --terminal --compact: to display a quick list of packages sorted by install date.
- 
- Note: "sudo python3 final.py --compact" will not work! You must provide an email address to it in order to receive sorted list of packages.
- Of course, you can also include terminal if you like, simply run "sudo python3 final.py --email address@illinois.edu --terminal --compact".
- sudo cat /var/log/messages : to view the syslog message.
- sudo ls /etc/cron.daily/ : to see the files in your crob.daily directory, make sure it has both wrap.sh and final.py.
- if you haven't, you can simply run "sudo mv final.py /etc/cron.daily/" and "sudo mv wrap.sh /etc/cron.daily/" to move final.py and wrap.sh from current directory to /etc/cron.daily directory respectively.
- make sure include "#!/bin/python3" on top of the final.py (first line), so it will execute as python3 script.
- Also, make sure to change the arguments in the wrap.sh file to receive email from that particular node. 
- you can simply do that by add email address to the end of the second line in wrap.sh from "/usr/bin/python3 final.py --email" to "/usr/bin/python3 final.py --email address@illinois.edu"
- sudo run-parts /etc/cron.daily/ : to run the cron job daily. If you want to run it hourly, simply change the cron.daily to cron.hourly, so it will be something like this, "sudo run-parts /etc/cron.hourly/".



## 10. Frequently Asked Questions (FAQ) - Optional
Compile a list of common questions users might have and their corresponding answers.
## 11. Version History - Optional
NA, right now, will update if changes rise.
## 12. Support and Contact Information - Optional
Contact Daniel Lapine 





